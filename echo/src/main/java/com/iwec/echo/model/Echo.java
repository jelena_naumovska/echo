package com.iwec.echo.model;

public class Echo {

	private String message;

	public Echo() {
		super();
	}

	public Echo(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
