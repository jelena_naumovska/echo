package com.iwec.echo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iwec.echo.model.Echo;

@RestController
@RequestMapping("/echo")
public class EchoController {

	private static final String template = "Hello, %s!";
	

	@RequestMapping(value = "/{name}", method = RequestMethod.GET, produces = "application/json")
	public Echo greeting(@RequestParam(value = "name", defaultValue = "") String name) {
		return new Echo(String.format(template, name));
	}
}