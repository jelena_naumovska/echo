#!/bin/bash

mv $echo-0.0.1-SNAPSHOT.war $CATALINA_HOME/webapps/$echo-0.0.1-SNAPSHOT.war
$CATALINA_HOME/bin/shutdown.sh
sleep 1
$CATALINA_HOME/bin/startup.sh
sleep 1

xdg-open http://localhost:8080/$echo-0.0.1-SNAPSHOT.war